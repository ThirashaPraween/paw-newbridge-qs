import { SquidWidget } from '@0xsquid/widget';
import { ConfigTheme } from '@0xsquid/widget/widget/core/types/config';
import { Box, Grid } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useIsDarkMode } from 'state/user/hooks';

const QUICKSWAP = 'Quickswap';
const QUICKSWAP_V3 = 'Quickswap_v3';

const SwapCrossChain: React.FC = () => {
  const darkMode = useIsDarkMode();
  const darkModeStyle: ConfigTheme = {
    baseContent: '#f3ecec',
    neutralContent: '#696c80',
    base100: '#0f0e0e',
    base200: '#232531',
    base300: '#0f1720',
    error: '#ED6A5E',
    warning: '#FFB155',
    success: '#62C555',
    primary: '#EA4745',
    secondary: '#DD5351',
    secondaryContent: '#2e2d30',
    neutral: '#121319',
    roundedBtn: '26px',
    roundedBox: '1rem',
    roundedDropDown: '20rem',
    displayDivider: true,
    advanced: {
      transparentWidget: true,

    },

  };
  const lightModeStyle: ConfigTheme = {
    baseContent: '#070002',
    neutralContent: '#070002',
    base100: '#FFFFFF',
    base200: '#e7e7e7',
    base300: '#FBF5FF',
    error: '#ED6A5E',
    warning: '#FFB155',
    success: '#62C555',
    primary: '#EA4745',
    secondary: '#DD5351',
    secondaryContent: '#F7F6FB',
    neutral: '#FFFFFF',
    roundedBtn: '26px',
    roundedBox: '1rem',
    roundedDropDown: '20rem',
    displayDivider: true,

  };
  useEffect(() => {
    // console.log('SwapCrossChain #init');
  }, []);

  return (
    <Grid style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
      <Box
        style={{
          width: '100%',
          maxWidth: '420px',
          border: "2px black solid",
          borderRadius: 15
        }}
      >
        <SquidWidget
          config={{
            mainLogoUrl: '',
            companyName: 'Pawswap',
            apiUrl: 'https://api.0xsquid.com',
            style: darkMode ? lightModeStyle : lightModeStyle,
            initialFromChainId: 1,
            initialToChainId: 137,
            favTokens: [
              {
                address: "0xdc63269ea166b70d4780b3a11f5c825c2b761b01",
                chainId: 1
              }
            ],
            loadPreviousStateFromLocalStorage: true,
            preferDex: [QUICKSWAP, QUICKSWAP_V3],
            titles: {
              swap: 'Cross-Chain',
              settings: "Settings",
              wallets: "Wallets",
              tokens: "Tokens",
              chains: "Chains",
              history: "History",
              transaction: "Transaction",
              destination: "Destination address",
              allTokens: 'All Tokens'
            },

          }}
        />
      </Box>
    </Grid>
  );
};

export default SwapCrossChain;
